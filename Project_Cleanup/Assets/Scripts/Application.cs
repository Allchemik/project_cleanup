﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Application : MonoBehaviour
{
    public Slider slider;
    public float tMax;
    private float _tPassed = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        _tPassed = 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        _tPassed += Time.deltaTime;
        slider.value = _tPassed / tMax * slider.maxValue;
        
        
    }
}
