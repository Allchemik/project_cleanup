﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PayerMoveController : MonoBehaviour {

    private float _vertical;
    private float _horizontal;
    private Rigidbody2D _Player;
    Vector3 _direction;
    float _rotation;
    private string _horizontalAxis;
    private string _verticalAxis;
    private string _actionButton;
    private string _pickButton;

    public float Speed;
    public int player;

    private void Awake()
    {
        SetControllerNumber();
    }

    // Use this for initialization
    void Start () {
        _Player = GetComponent<Rigidbody2D>();
        _rotation = 0.0f;
        _direction = Vector2.zero;
    }
	
	// Update is called once per frame
	void Update () {
        
        float angle = Mathf.Atan2(_direction.x, _direction.y) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }
    private void FixedUpdate()
    {
        _vertical = Input.GetAxis(_verticalAxis);
        _horizontal = Input.GetAxis(_horizontalAxis);

        if (Input.GetButtonDown(_actionButton))
        {
            Debug.Log(_actionButton);
        }

        if (Input.GetButtonDown(_pickButton))
        {
            Debug.Log(_pickButton);
        }

        if (Mathf.Abs(_vertical) < 0.5f)
            _vertical = 0.0f;

        if (Mathf.Abs(_horizontal) < 0.5f)
            _horizontal = 0.0f;

        _direction = new Vector2( -_horizontal , _vertical);

        _Player.velocity = new Vector2(_horizontal, _vertical)*Speed;
    }

    internal void SetControllerNumber ()
    {
        _horizontalAxis = "J" + player + "Horizontal";
        _verticalAxis = "J" + player + "Vertical";
        _actionButton = "J" + player + "Action";
        _pickButton = "J" + player + "Pick";
    }
}
